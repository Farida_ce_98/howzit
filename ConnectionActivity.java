package com.example.faridam.howzit;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import static com.example.faridam.howzit.ScanActivity.index;

public class ConnectionActivity extends AppCompatActivity {
    Button listen ,send,listDevices;
    ListView listView;
    TextView msg_box,status;
    EditText writeMsg;
    BluetoothAdapter bluetoothAdapter;
    static  final int STATE_LISTENING = 1 ;
    static  final int STATE_CONNECTING = 2;
    static  final int STATE_CONNECTED = 3 ;
    static  final int STATE_CONNECTION_FAILED = 4;
    static  final int STATE_MESSAGE_RECIEVED = 5 ;

    int REQUEST_ENABLE_BLIUETOOTH = 1 ;
    SendRecieve sendRecieve;
   // BluetoothDevice[] btArray;
    public  static BluetoothDevice device ;

    private static final String APP_NAME = "Howzit";
    private static final UUID MY_UUID = UUID.fromString( "68f9c894-cd95-11ea-87d0-0242ac130003" );



    private static final String TAG = "DeviceListActivity";
    private static final int REQUEST_ENABLE_BT =2 ;

    /**
     * Return Intent extra
     */
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    /**
     * Member fields
     */
    public static BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
    public static ArrayList<String> arrayList = new ArrayList<>();
    public static ArrayList<String> tmp = new ArrayList<>();

    /**
     * Newly discovered devices
     */
    ArrayAdapter<String> mNewDevicesArrayAdapter;

    ArrayAdapter<String> ttt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Intent discoverableIntent =
                new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(discoverableIntent);*/

        // Setup the window
      //  requestWindowFeature( Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.start_connection);

        // Set result CANCELED in case the user backs out
      //  setResult( Activity.RESULT_CANCELED);

        // Initialize the button to perform device discovery
        //Button scanButton = (Button) findViewById(R.id.button_scan);
        //unregisterReceiver( mReceiver );
        doDiscovery();
        findViewByIdies();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!bluetoothAdapter.isEnabled()){
            Intent enableIntent = new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE );
            startActivityForResult( enableIntent , REQUEST_ENABLE_BLIUETOOTH );
        }
        implementListeners();
       /* scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                doDiscovery();
                v.setVisibility(View.GONE);
            }
        });*/

        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices
       // ArrayAdapter<String> pairedDevicesArrayAdapter =
          //      new ArrayAdapter<String>(this, R.layout.device_name);


        mNewDevicesArrayAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,arrayList);
        //mNewDevicesArrayAdapter = new ArrayAdapter<String>();


        // Find and set up the ListView for paired devices
            /*ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
            pairedListView.setAdapter(pairedDevicesArrayAdapter);
            pairedListView.setOnItemClickListener(mDeviceClickListener);*/

        // Find and set up the ListView for newly discovered devices
            /*ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
            newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
            newDevicesListView.setOnItemClickListener(mDeviceClickListener);*/

        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        int count = mNewDevicesArrayAdapter.getCount();



        /*for (int i =0 ;i<mNewDevicesArrayAdapter.getCount();i++) {
            String str = mNewDevicesArrayAdapter.getItem(i).toString();
            arrayList.add(str);

            System.out.println(str);
        }*/



        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        // Get a set of currently paired devices
       // Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
       /* if (pairedDevices.size() > 0) {
          //  findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
            for (BluetoothDevice device : pairedDevices) {
                pairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } else {
            String noDevices = getResources().getText(R.string.none_paired).toString();
            mNewDevicesArrayAdapter.add(noDevices);
            pairedDevicesArrayAdapter.add(noDevices);
        }*/

    }
    private void implementListeners() {
        listDevices.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setAdapter( ttt );
                System.out.println("kir khar: "+ arrayList.size());
               /* Set<BluetoothDevice> bt = bluetoothAdapter.getBondedDevices();
                String [] strings = new String [bt.size()];
                btArray = new BluetoothDevice[bt.size()];
                int index = 0 ;
                if (bt.size()>0){
                    for(BluetoothDevice device : bt){
                        System.out.println(device.getName());
                        btArray[index] = device;
                        strings[index] = device.getName();
                        index++;
                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,strings);
                    listView.setAdapter( arrayAdapter);
                }*/
            }
        } );
        listen.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ServerClass serverClass = new ServerClass();
                    serverClass.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } );
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              //  unregisterReceiver( mReceiver );
                ClientClass clientClass = new ClientClass( device );
                clientClass.start();

                status.setText( "Connecting" );
            }
        } );

        send.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = String.valueOf( writeMsg.getText() );
                Log.d( "sended" , string );
                sendRecieve.write( string.getBytes() );
            }
        } );
    }
    private void findViewByIdies() {
        listen = (Button) findViewById( R.id.listen);
        send = (Button) findViewById( R.id.send );
        listDevices = (Button) findViewById( R.id.listDevices);
        listView  = (ListView) findViewById( R.id.listview);
        msg_box = (TextView) findViewById( R.id.msg );
        status = (TextView) findViewById( R.id.status);
        writeMsg = (EditText) findViewById( R.id.writemsg);

    }

    Handler handler = new Handler( new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case STATE_LISTENING:
                    status.setText( "Listenning");
                    break;
                case STATE_CONNECTED:
                    status.setText( "Connected");
                    break;
                case STATE_CONNECTING:
                    status.setText( "Connecting");
                    break;
                case STATE_CONNECTION_FAILED:
                    status.setText( "Connection failed");
                    break;
                case STATE_MESSAGE_RECIEVED:
                    byte[] readbuff = (byte[]) msg.obj;
                    //System.out.println("gsv");
                    //System.out.println(readbuff.toString());
                    String tempMsg = new String( readbuff,0,msg.arg1 );
                    //og.d( "mymessage" , tempMsg );
                    msg_box.setText( tempMsg );
                    break;
            }
            return true;
        }
    } );

    private class ServerClass extends Thread{
        private BluetoothServerSocket serverSocket ;
        public ServerClass() throws IOException {
            serverSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord( APP_NAME,MY_UUID );
        }
        public void run(){
            BluetoothSocket socket = null;
            while (socket == null) {
                try {
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTING;
                    handler.sendMessage( message);
                    socket = serverSocket.accept();

                } catch (IOException e) {

                    e.printStackTrace();
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTION_FAILED;
                    handler.sendMessage( message);
                }
                if(socket!=null){
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTED;
                    handler.sendMessage( message);

                    sendRecieve = new SendRecieve(socket);
                    sendRecieve.start();
                    break;
                }
            }
        }
    }

    private class ClientClass extends Thread {

        private BluetoothDevice device;
        private BluetoothSocket socket;

        public ClientClass(BluetoothDevice device1) {
            device = device1;
            try {
                socket = device.createRfcommSocketToServiceRecord( MY_UUID );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                socket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                handler.sendMessage( message );

                sendRecieve = new SendRecieve( socket );
                sendRecieve.start();

            } catch (IOException e) {

                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage( message );
                e.printStackTrace();
            }
        }

    }
    private class SendRecieve extends Thread{
        private final BluetoothSocket bluetoothSocket ;
        private final InputStream inputStream;
        private final OutputStream outputStream;

        public SendRecieve(BluetoothSocket socket){
            bluetoothSocket = socket;
            InputStream tempIn = null;
            OutputStream tempOut = null;

            try {
                tempIn = bluetoothSocket.getInputStream();
                tempOut = bluetoothSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            inputStream = tempIn;
            outputStream = tempOut;

        }
        public void run(){
            byte[] buffer = new byte[1024];
            int bytes;
            while (true){
                try {
                    bytes =  inputStream.read(buffer);
                    //Log.d( "rescieved" , buffer.toString());
                    handler.obtainMessage(STATE_MESSAGE_RECIEVED,bytes,-1,buffer).sendToTarget();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        public void write( byte[] bytes){
            try {
                outputStream.write( bytes );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        Log.d(TAG, "doDiscovery()");

        // Indicate scanning in the title
        setProgressBarIndeterminateVisibility(true);
        //setTitle(R.string.scanning);

        // Turn on sub-title for new devices
       // findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    /**
     * The on-click listener for all devices in the ListViews
     */
   /* private AdapterView.OnItemClickListener mDeviceClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Cancel discovery because it's costly and we're about to connect
            mBtAdapter.cancelDiscovery();

            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            // Create the result Intent and include the MAC address
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

            // Set result and finish this Activity
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };*/

    /**
     * The BroadcastReceiver that listens for discovered devices and changes the title when
     * discovery is finished
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    if(!arrayList.contains( device.getAddress() )){
                    mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                    //btArray[0] = device;
                   // arrayList.add(device.getAddress());
                    //Log.d("kiiiiiiir" , (String)arrayList.size());
                    }
                    System.out.println("ammm: "+ arrayList.size());
                    // String uid= ScanActivity.arrayListAdapter.get( index).uid;
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                setTitle(R.string.select_device);
                if (mNewDevicesArrayAdapter.getCount() == 0) {
                    String noDevices = getResources().getText(R.string.none_found).toString();
                    mNewDevicesArrayAdapter.add(noDevices);
                }
            }
        }
    };
}

